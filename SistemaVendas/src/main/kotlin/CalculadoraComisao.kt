import java.math.BigInteger
import kotlin.math.floor

class CalculadoraComisao {
    fun calcular(valorVenda: Double): Double {

        var comissao = if (valorVenda <= 10000) valorVenda * 0.05 else valorVenda * 0.06

        return truncar(comissao);
    }

    private fun truncar(valorVenda: Double) = floor(valorVenda * 100) / 100
}
