import dao.VendaRepository
import modelo.Venda
import kotlin.math.floor

open class CalculadoraRoyalties {

    private var repositorio: VendaRepository

    constructor(){
        repositorio = VendaRepository()
    }

    constructor(repositorioIndicado: VendaRepository){
        repositorio = repositorioIndicado
    }

    fun calcular(mes: Int, ano: Int): Double {

        val vendas = obterVendas(mes, ano)
        val faturamento = obterFaturamento(vendas)
        val totalComissao = obterComissao(vendas)

        val royalties = (faturamento - totalComissao) * 0.2

        return truncarComDuasCasas(royalties)

    }

    private fun truncarComDuasCasas(royalties: Double): Double {
        return floor(royalties * 100) /100
    }

    private fun obterComissao(vendas: List<Venda>): Double {

        var totalComissao = 0.00

        vendas.forEach {
            totalComissao += CalculadoraComisao().calcular(it.valor)
        }

        return totalComissao
    }

    private fun obterFaturamento(vendas: List<Venda>): Double {

        var faturamento = 0.00

        vendas.forEach {
            faturamento += it.valor
        }

        return faturamento
    }

    private fun obterVendas(mes: Int, ano: Int): List<Venda> {
        return repositorio.obterVendasPorMesEAno(mes, ano)
    }
}
