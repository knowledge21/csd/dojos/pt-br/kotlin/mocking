import dao.VendaRepository
import modelo.Venda
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import kotlin.test.assertEquals

class TesteCalculadoraRoyalties {

    private lateinit var marionete: VendaRepository

    @BeforeEach
    fun criarMarionete()
    {
        marionete = mock(VendaRepository::class.java)
    }

    @Test
    fun teste_calcula_royalties_de_0_reais_para_mes_sem_vendas(){

        //Arrange
        val ano = 2020
        val mes = 2
        val royaltiesEsperados = 0.00

        //Act
        val royaltiesCalculados = CalculadoraRoyalties().calcular(mes, ano)

        //Assert
        assertEquals(royaltiesEsperados, royaltiesCalculados)

    }

    @Test
    fun teste_calcula_royalties_de_388_55_reais_para_mes_com_uma_venda_de_2045(){

        //Arrange
        val ano = 2019
        val mes = 3
        val royaltiesEsperados = 388.55

        val listaFalsa = listOf<Venda>(Venda(1, mes, ano, 2045.00))

        `when`(marionete.obterVendasPorMesEAno(mes, ano)).thenReturn(listaFalsa)

        //Act
        val royaltiesCalculados = CalculadoraRoyalties(marionete).calcular(mes, ano)

        //Assert
        assertEquals(royaltiesEsperados, royaltiesCalculados)

    }

    @Test
    fun teste_calcula_royalties_de_75670_62_reais_para_mes_com_vendas_de_2159_22_e_400321_12(){

        //Arrange
        val ano = 2021
        val mes = 3
        val royaltiesEsperados = 75670.62

        val listaFalsa = listOf<Venda>(Venda(1, mes, ano, 2159.22),
                                        Venda(1, mes, ano, 400321.12))

        `when`(marionete.obterVendasPorMesEAno(mes, ano)).thenReturn(listaFalsa)

        //Act
        val royaltiesCalculados = CalculadoraRoyalties(marionete).calcular(mes, ano)

        //Assert
        assertEquals(royaltiesEsperados, royaltiesCalculados)

    }
}