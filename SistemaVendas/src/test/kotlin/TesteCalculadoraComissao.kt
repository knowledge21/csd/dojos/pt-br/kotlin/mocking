import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TesteCalculadoraComissao {

    @Test
    fun teste_calcula_na_faixa_de_5_pct_comissao_de_500_para_venda_de_10K() {

        // Arrange
        val valorVenda = 10000.00
        val comissaoEsperada = 500.00

        // Act
        val comissaoCalculada = CalculadoraComisao().calcular(valorVenda)

        //Assert
        assertEquals(comissaoEsperada, comissaoCalculada)
    }

    @Test
    fun teste_calcula_na_faixa_de_5_pct_comissao_de_50_para_venda_de_1K() {

        // Arrange
        val valorVenda = 1000.00
        val comissaoEsperada = 50.00

        // Act
        val comissaoCalculada = CalculadoraComisao().calcular(valorVenda)

        //Assert
        assertEquals(comissaoEsperada, comissaoCalculada)
    }

    @Test
    fun teste_calcula_na_faixa_de_6_pct_comissao_de_600_60_para_venda_de_10010() {

        // Arrange
        val valorVenda = 10010.00
        val comissaoEsperada = 600.60

        // Act
        val comissaoCalculada = CalculadoraComisao().calcular(valorVenda)

        //Assert
        assertEquals(comissaoEsperada, comissaoCalculada)
    }

    @Test
    fun teste_calcula_na_faixa_de_5_pct_comissao_de_2_77_para_venda_de_55_59() {

        // Arrange
        val valorVenda = 55.59
        val comissaoEsperada = 2.77

        // Act
        val comissaoCalculada = CalculadoraComisao().calcular(valorVenda)

        //Assert
        assertEquals(comissaoEsperada, comissaoCalculada)
    }

    @Test
    fun teste_calcula_na_faixa_de_6_pct_comissao_de_603_33_para_venda_de_10055_59() {

        // Arrange
        val valorVenda = 10055.59
        val comissaoEsperada = 603.33

        // Act
        val comissaoCalculada = CalculadoraComisao().calcular(valorVenda)

        //Assert
        assertEquals(comissaoEsperada, comissaoCalculada)
    }
}